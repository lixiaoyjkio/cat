### 项目亮点：
1、uniapp模块化开发，结合renderjs，vuex状态管理
2、配置一套符合团队习惯且规范的husky
3、localStorage的时效性、私密性、命名规范
4、websocket聊天时效缓存
5、uniapp页面的美化设计
6、为了减少请求，搜索时使用防抖优化，点击节流优化
7、聊天表情，图片，视频，声音的数值化处理 8、MongoDB数据数据字段的关系设计