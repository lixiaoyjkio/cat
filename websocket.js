import Vue from 'vue'

 const serverUrl = 'http://localhost:3000'
// const serverUrl = 'http://10.16.51.78:3000'
const socket = new WebSocket("ws://localhost:8001")
// const socket = new WebSocket("ws://10.16.51.78:8001")
const value = uni.getStorageSync('user')
Vue.prototype.socket = socket
// main中注册用户
socket.onopen = function(e) {
	socket.send(
		JSON.stringify({
			uid: value.id,
			type: 1,
			nickname: value.name,
			msg: '',
			bridge: [],
			groupId: []
		})
	);

	const pone = new Promise(function(resolve, reject) {
		// 所有的群
		uni.request({
			url: serverUrl + '/group/getAllGroup',
			data: {
				uid: value.id,
			},
			method: 'POST',
			success: (data) => {
				if(data.data.status == 200){
					const res = data.data.result
					if(res !== null){
						resolve(res)
					}
				}else{
					reject()
				}
			},
		});

	}).then(function(data) {
		if(data.length !== 0){ // 有群
			data.forEach((item, index) => {
				// 创建群 加入群
				if(item !== null){
					socket.send(
						JSON.stringify({
							uid: value.id,
							type: 10,
							nickname: '',
							groupName: item.name,
							bridge: [],
							groupId: item._id, // 群id只有一个，但是我要用我的userid当群主
						})
					)
				}
				
			})
		}
		
	})
};



socket.onclose = function(e) {
	console.log("服务器关闭");
};
socket.onerror = function() {
	console.log("连接出错");
};
socket.onmessage = function(e) {
	let message = JSON.parse(e.data)
	// console.log(message)
};
