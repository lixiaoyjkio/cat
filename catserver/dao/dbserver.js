var dbmodel = require("../model/dbmodel")
var bcrypt = require("../dao/bcrypt")
var User = dbmodel.model("User")
var Friend = dbmodel.model("Friend")
var Group = dbmodel.model("Group")
var GroupUser = dbmodel.model("GroupUser")
var Message = dbmodel.model("Message")
var GroupMsg = dbmodel.model("GroupMsg")
var jwt = require("../dao/jwt")
const { Promise } = require("mongoose")
// 新建用户
exports.buildUser = function (name, mail, pwd, res) {
    // 密码加密
    let password = bcrypt.encryption(pwd)
    let data = {
        name: name,
        email: mail,
        pwd: password,
        time: new Date(),
    }
    let user = new User(data)
    user.save(function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 匹配用户和邮箱表项目
exports.countUserValue = function (data, type, res) {
    let wherester = {}
    if (type === "name") {
        wherester = { name: data }
        User.countDocuments(wherester, function (err, result) {
            if (err) {
                res.send({ status: 500 })
            } else {
                res.send({ status: 200, result })
            }
        })
    } else if (type === "email") {
        wherester = { email: data }
        User.countDocuments(wherester, function (err, result) {
            if (err) {
                res.send({ status: 500 })
            } else {
                res.send({ status: 200, result })
            }
        })
    } else {
        res.send({ status: 500 })
    }
}
// 用户验证
exports.userMath = function (data, pwd, res) {
    let wherestr = { $or: [{ name: data }, { email: data }] }
    let out = { name: 1, imgurl: 1, pwd: 1 }
    User.find(wherestr, out, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            if (result == "") {
                res.send({ status: 400 })
            }
            result.map(function (e) {
                const pwdMath = bcrypt.verification(pwd, e.pwd)

                if (pwdMath) {
                    let token = jwt.generateToken(e._id)
                    let back = {
                        id: e._id,
                        name: e.name,
                        imgurl: e.imgurl,
                        token: token,
                    }
                    res.send({ status: 200, back })
                } else {
                    res.send({ status: 400 })
                }
            })
        }
    })
}
// 搜索用户
exports.searchUser = function (data, res) {
    let wherestr
    if (data == "cat") {
        wherestr = {}
    } else {
        wherestr = {
            $or: [
                { name: { $regex: data.data } },
                { email: { $regex: data.data } },
            ],
        }
    }
    let out = {
        name: 1,
        email: 1,
        imgurl: 1,
    }
    User.find(wherestr, out, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 判断是否为好友
exports.isFriend = function (uid, fid, res) {
    let wherestr = { userId: uid, friendId: fid, state: 0 }
    Friend.findOne(wherestr, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            if (result) {
                res.send({ statusbool: true })
            } else {
                res.send({ statusbar: false })
            }
        }
    })
}
// 搜索群
exports.searchGroup = function (data, res) {
    let wherestr
    if (data == "cat") {
        wherestr = {}
    } else {
        wherestr = {
            name: { $regex: data },
        }
    }
    let out = {
        name: 1,
        imgurl: 1,
    }
    Group.find(wherestr, out, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 判断是否在群里
exports.isInGroup = function (uid, gid, res) {
    let wherestr = { userId: uid, groupId: gid }
    Friend.findOne(wherestr, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            if (result) {
                res.send({ status: 200 })
            } else {
                res.send({ status: 400 })
            }
        }
    })
}
// 用户详情
exports.userDetail = function (uid, res) {
    let wherestr = { _id: uid }
    let out = { pwd: 0 }
    User.findOne(wherestr, out, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 用户信息修改
function update(data, update, res) {
    // 抽离更新函数
    User.findByIdAndUpdate(data, update, function (err, resu) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}

exports.userUpdate = function (data, res) {
    let updatestr = {}
    // 判断是否有密码
    if (typeof data.pwd != "undefined") {
        // 有密码进行匹配
        User.find({ _id: data.id }, { pwd: 1 }, function (err, result) {
            if (err) {
                res.send({ status: 500 })
            } else {
                if (result == "") {
                    res.send({ status: 400 })
                }
                result.map(function (e) {
                    const pwdMath = bcrypt.verification(data.pwd, e.pwd)
                    // 密码验证成功
                    if (pwdMath) {
                        // 如果为修改密码先加密
                        if (data.type == "pwd") {
                            let password = bcrypt.encryption(data.data)
                            updatestr[data.type] = password
                            update(data.id, updatestr, res)
                        } else {
                            updatestr[data.type] = data.data
                            // 邮箱匹配
                            User.countDocuments(
                                updatestr,
                                function (err, result) {
                                    if (err) {
                                        res.send({ status: 500 })
                                    } else {
                                        // 没有匹配项可以修改
                                        if (result == 0) {
                                            update(data.id, updatestr, res)
                                        } else {
                                            res.send({ status: 300 })
                                        }
                                    }
                                }
                            )
                        }
                        // update(data.id, updatestr, res)
                    } else {
                        // 密码匹配失败
                        res.send({ status: 400 })
                    }
                })
            }
        })
    } else if (data.type == "name") {
        // 如果是用户名先进行匹配
        updatestr[data.type] = data.data
        User.countDocuments(updatestr, function (err, result) {
            if (err) {
                res.send({ status: 500 })
            } else {
                // 没有匹配项可以修改
                if (result == 0) {
                    update(data.id, updatestr, res)
                } else {
                    res.send({ status: 300 })
                }
            }
        })
    } else {
        // 一般项目修改
        updatestr[data.type] = data.data
        update(data.id, updatestr, res)
    }
}
// 修改好友昵称
exports.updataMarkName = function (data, res) {
    let wherestr = { userId: data.uid, friendId: data.fid }
    let updatestr = { markname: data.name }
    Friend.updateOne(wherestr, updatestr, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 获取好友昵称
exports.getMarkName = function (data, res) {
    let wherestr = { userId: data.uid, friendId: data.fid }
    let out = { markname: 1 }
    Friend.findOne(wherestr, out, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 好友操作

// 添加好友
exports.bulidFriend = function (uid, fid, state, res) {
    let data = {
        userId: uid,
        friendId: fid,
        state: state,
        time: new Date(),
        lastTime: new Date(),
    }
    let friend = new Friend(data)
    friend.save(function (err, result) {
        if (err) {
            console.log("申请好友出错")
        } else {
            // res.send({ status: 200 })
        }
    })
}
// 好友最后通讯时间
exports.upFriendLastTime = function (data) {
    let wherestr = {
        $or: [
            { userId: data.uid, friendId: data.fid },
            { userId: data.fid, friendId: data.uid },
        ],
    }
    let updatestr = { lastTime: new Date() }
    Friend.updateMany(wherestr, updatestr, function (err, result) {
        if (err) {
            // res.send({ status: 500 })
            console.log("更新好友最后通讯时间出错")
        } else {
            // res.send({ status: 200 })
        }
    })
}

//添加一天消息

// 添加一对一消息
exports.insertMsg = function (uid, fid, msg, type, res) {
    let data = {
        userId: uid,
        friendId: fid,
        message: msg,
        types: type,
        time: new Date(),
        state: 1,
    }
    let message = new Message(data)
    message.save(function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// 好友申请
exports.applyFriend = function (data, res) {
    // 判断是否已经申请过
    let wherestr = { userId: data.uid, friendId: data.fid }
    Friend.countDocuments(wherestr, (err, result) => {
        if (err) {
            res.send({ status: 500 })
        } else {
            // 如果result = 0 为初次申请
            if (result == 0) {
                this.bulidFriend(data.uid, data.fid, 2)
                this.bulidFriend(data.fid, data.uid, 1)
            } else {
                // 已经为好友
                this.upFriendLastTime(data)
            }
            // 添加消息
            this.insertMsg(data.uid, data.fid, data.msg, 0, res)
        }
    })
}
// 更新好友状态
exports.updateFriendState = function (data, res) {
    let wherestr = {
        $or: [
            { userId: data.uid, friendId: data.fid },
            { userId: data.fid, friendId: data.uid },
        ],
    }
    Friend.updateMany(wherestr, { state: 0 }, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// 拒绝好友 删除好友
exports.deleteFriend = function (data, res) {
    let wherestr = {
        $or: [
            { userId: data.uid, friendId: data.fid },
            { userId: data.fid, friendId: data.uid },
        ],
    }
    Friend.deleteMany(wherestr, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// 按需求获取用户列表
exports.getUsers = function (data, res) {
    // 拿到用户
    let query = Friend.find({})
    // 查询条件
    query.where({ userId: data.uid, state: data.state })
    // 查询friendId 关联user对象
    query.populate("friendId")
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (e) {
            let result = e.map(function (ver) {
                return {
                    id: ver.friendId._id,
                    name: ver.friendId.name,
                    markname: ver.markname,
                    imgurl: ver.friendId.imgurl,
                    lastTime: ver.lastTime,
                }
            })

            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 获取一对一消息
exports.getOneMsg = function (data, res) {
    let query = Message.findOne({})
    // 查询条件
    query.where({
        $or: [
            { userId: data.uid, friendId: data.fid },
            { userId: data.fid, friendId: data.uid },
        ],
    })
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ time: -1 })
    // 查询结果
    query
        .exec()
        .then(function (ver) {
            let result = {
                message: ver.message,
                time: ver.time,
                types: ver.types,
            }
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 汇总一对一消息
exports.unreadMsg = function (data, res) {
    let wherestr = { userId: data.fid, friendId: data.uid, state: 1 } // 倒置好友未读消息位置
    Message.countDocuments(wherestr, (err, result) => {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 一对一消息状态修改
exports.updateMsg = function (data, res) {
    let wherestr = { userId: data.fid, friendId: data.uid, state: 1 } // 倒置id 删除好友发的信息
    // 修改内容
    let updatestr = { state: 0 }

    Message.updateMany(wherestr, updatestr, (err, result) => {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// 获取群列表加入的群
exports.getGroup = function (data, res) {
    // id 为用户 所在的群
    let query = GroupUser.find({})
    // 查询条件
    query.where({ userId: data.uid })
    // 查询friendId 关联user对象
    query.populate("groupId")
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (e) {
            let result = e.map(function (ver) {
                return {
                    gid: ver.groupId._id,
                    name: ver.groupId.name,
                    markname: ver.name,
                    imgurl: ver.groupId.imgurl,
                    lastTime: ver.lastTime,
                    tip: ver.tip,
                }
            })
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 获取群主的群
exports.getqunzhuGroup = function (data, res) {
    // id 为用户 所在的群
    let query = Group.find({})
    // 查询条件
    query.where({ userId: data.uid })
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (result) {
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 拿到所有的群
exports.getAllGroup = async function (data, res) {
    try {

    // 自己的群
    let wherestrTwo = { userId: data.uid }
    const resultTwo = await Group.find(wherestrTwo)

    // 加入的群
    let wherestrOne = { userId: data.uid }
    const resultOne = await GroupUser.find(wherestrOne)
    // 拿到加入的群的群号
    const taskPromist =  resultOne.map(async function (item, index) {
        resultTwo.push(await Group.findOne({ _id: item.groupId }))
    })
    await Promise.all(taskPromist) // 执行完所有的Promist才能向下执行
    let result = resultTwo
    res.send({ status: 200, result })
    } catch (error) {
        res.send({ status: 500 })
    }
}
//
// 按要求获取群消息
exports.getOneGroupMsg = function (data, res) {
    // console.log(data.gid)
    var query = GroupMsg.findOne({})
    // 查询条件
    query.where({ groupId: data.gid })
    // 关联user对象
    query.populate("userId")
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ time: -1 })
    // 查询结果
    query
        .exec()
        .then(function (result) {         
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 群消息状态修改
exports.updateGroupMsg = function (data, res) {
    let wherestr = { userId: data.uid, groupId: data.gid }
    // 修改内容
    let updatestr = { tip: 0 }

    Message.countDocuments(wherestr, updatestr, (err, result) => {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// 获取好友所有聊天消息
exports.getAllmsg = async function (data, res) {
    // 拿到用户信息
    async function getname(id) {
        let wherestrUser = { _id: id }
        return await User.findOne(wherestrUser)
    }

    const userName = await getname(data.uid).then(function (result) {
        return result.name
    })
    const friendName = await getname(data.fid).then(function (result) {
        if (result) {
            return
        }
        return result.name
    })
    // 拿到消息
    let query = Message.find()
    // 查询条件 uid fid
    query.where({
        $or: [
            { userId: data.uid, friendId: data.fid },
            { userId: data.fid, friendId: data.uid },
        ],
    })
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ time: -1 })
    // 查询结果
    query
        .exec()
        .then(function (ver) {
            const resultList = []
            ver.forEach((element, index) => {
                const result = {}
                if (element.userId == data.uid) {
                    // 用户发给好友的
                    result.userName = userName
                    result.friendName = friendName
                    result.userdetailMsg = element
                    resultList.push(result)
                } else {
                    // 好友发给的用户
                    result.userName = friendName
                    result.friendName = userName
                    result.userdetailMsg = element
                    resultList.push(result)
                }
            })
            return resultList
        })
        .then(function (ver) {
            res.send({ status: 200, ver })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 发送一对一消息
exports.sendOneMsg = function (data, res) {
    // 先标记为已读消息
    let savaData = {
        userId: data.uid,
        friendId: data.fid,
        message: data.msg,
        types: data.type,
        time: new Date(),
        state: 0,
    }
    let message = new Message(savaData)
    message.save(function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// websocket send 消息
exports.websocketSendOneMsg = function (obj) {
    // 先标记为已读消息
    let saveDaet = {
        userId: obj.bridge[0],
        friendId: obj.bridge[1],
        message: obj.msg,
        types: "0",
        time: new Date(),
        state: 1,
    }
    let message = new Message(saveDaet)
    message.save(function (err, result) {
        if (err) {
            console.log(err)
        }
    })
}
// websocket sendGroup 消息
exports.websocketSendGroupMsg = function (obj) {
    // 先标记为已读消息
    let savaData = {
        groupId: obj.groupId,
        userId: obj.uid,
        message: obj.msg,
        types: obj.type,
        time: new Date(),
    }
    let groupMsg = new GroupMsg(savaData)
    groupMsg.save(function (err, result) {
        if (err) {
            res.send({ status: 500 })
        }
    })
}
// 拿到用户的名字
exports.getUserName = function (data, res) {
    // 拿到用户信息
    let wherestrUser = { _id: data.id }
    User.findOne(wherestrUser, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 用户信息电脑修改
exports.updateSmhima = function (data, res) {
    // 抽离更新函数
    let wherestrUser = { _id: data.uid }
    let update = {}
    switch (data.where) {
        case "phone":
            update = { phone: data.value }
            break
        case "explain":
            update = { explain: data.value }
            break
        case "birth":
            update = { birth: data.value }
            break
        case "sex":
            update = { sex: data.value }
    }

    User.findByIdAndUpdate(wherestrUser, update, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200 })
        }
    })
}
// 创建群
exports.createGroupUser = function (data, res) {
    // 创建群
    new Promise(function (resolve, reject) {
        let groupData = {
            userId: data.uid,
            name: data.name + "的群",
            time: new Date(),
        }
        let group = new Group(groupData)
        group.save(function (err, result) {
            if (err) {
                res.send({ status: 500 })
            } else {
                data.groupId = result._id
                data.groupName = result.name
                resolve(data)
            }
        })
    }).then(function (data) {
        // 加入选择的成员
        let isall = true
        data.menber.forEach((item, index) => {
            let groupMemberData = {
                groupId: data.groupId,
                userId: item.id,
                name: item.name,
                tip: 0,
                time: new Date(),
                last: new Date(),
                shield: 0,
            }
            let groupuser = new GroupUser(groupMemberData)

            groupuser.save(function (err, result) {
                if (err) {
                    isall = false
                    res.send({ status: 500 })
                } else {
                }
            })
        })
        if (isall == true) {
            res.send({ status: 200 })
        }
    })
}
// 获取群信息
exports.getGroupName = function (data, res) {
    // 拿到用户信息
    let wherestrUser = { _id: data.gid }
    Group.findOne(wherestrUser, function (err, result) {
        if (err) {
            res.send({ status: 500 })
        } else {
            res.send({ status: 200, result })
        }
    })
}
// 发送群消息
// exports.sendOneGroupMsg = function (data, res) {

// }
// 获取我的群成员
exports.getgroupMeber = function (data, res) {
    // 拿到用户
    let query = GroupUser.find({})
    // 查询条件
    query.where({ groupId: data.gid })
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (e) {
            res.send({ status: 200, e })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 获取这个群里的所有成员
exports.getgroupMeberDetail = function (data, res) {
    // 拿到用户
    let query = GroupUser.find({})
    // 查询条件
    query.where({ groupId: data.gid })
    // 排序方式 最有通讯时间倒叙排序
    query.populate("userId")

    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (result) {
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 获取群主详细信息
exports.getgroupMeberqnzuDetail = function (data, res) {
    // id 为用户 所在的群
    let query = Group.find({})
    // 查询条件
    query.where({ _id: data.gid })
    // 排序方式 最有通讯时间倒叙排序
    query.populate("userId")

    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (result) {
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
// 获取群消息
exports.getGroupMsg = function (data, res) {
    // 拿到用户
    let query = GroupMsg.find({})
    // 查询条件
    query.where({ groupId: data.gid })
    // 排序方式 最有通讯时间倒叙排序
    query.sort({ lastTime: -1 })
    // 查询结果
    query
        .exec()
        .then(function (result) {
            res.send({ status: 200, result })
        })
        .catch(function (err) {
            res.send({ status: 500 })
        })
}
