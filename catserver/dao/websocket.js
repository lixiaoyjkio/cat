const moment = require("moment") // 时间处理

var dbserver = require("../dao/dbserver")

module.exports = function (ws) {
    // websocket 处理
    let users = []
    var conns = {}
    let groups = []

    function boardcast(obj) {
        
        // 单聊
        if (obj.bridge && obj.bridge.length) {
            dbserver.websocketSendOneMsg(obj) // 保存消息
            // 好友不在线不能发
            obj.bridge.forEach((item) => {
                if (conns[item]) {
                    conns[item].sendText(JSON.stringify(obj))
                } else {
                    return
                }
            })
            return
        }
        // 群
        if (obj.groupId) {
            dbserver.websocketSendGroupMsg(obj) // 保存消息
            console.log(groups)
            let group = groups.filter((item) => {
                return item.id === obj.groupId
            })[0]
            // console.log(group.users.length)
            group.users.forEach((item) => {
                // 好友不在线不能发
                console.log(item)
                if (conns[item.uid]) {
                    conns[item.uid].sendText(JSON.stringify(obj))
                } else {
                    return
                }
                
            })
            return
        }
        // 广播
        server.connections.forEach((conn, index) => {
            conn.sendText(JSON.stringify(obj))
        })
    }
    var server = ws
        .createServer(function (conn) {
            console.log("开始建立连接")
            conn.on("text", function (obj) {
                obj = JSON.parse(obj)
                conns[obj.uid] = conn
                switch (obj.type) {
                    // 创建连接
                    case 1:
                        let isuser = users.some((item) => {
                            return item.uid === obj.uid
                        })
                        if (!isuser) {
                            users.push({
                                nickname: obj.nickname,
                                uid: obj.uid,
                                status: 1,
                            })
                        } else {
                            users.map((item, index) => {
                                if (item.uid === obj.uid) {
                                    item.status = 1
                                }
                                return item
                            })
                        }
                        boardcast({
                            type: 1,
                            date: moment().format("YYYY-MM-DD HH:mm:ss"),
                            msg: obj.nickname + "加入聊天室",
                            users: users,
                            groups: groups,
                            uid: obj.uid,
                            nickname: obj.nickname,
                            bridge: obj.bridge,
                        })
                        break
                    // 注销
                    case 2:
                        users.map((item, index) => {
                            if (item.uid === obj.uid) {
                                item.status = 0
                            }
                            return item
                        })
                        boardcast({
                            type: 1,
                            date: moment().format("YYYY-MM-DD HH:mm:ss"),
                            msg: obj.nickname + "退出了聊天室",
                            users: users,
                            groups: groups,
                            uid: obj.uid,
                            nickname: obj.nickname,
                            bridge: [],
                        })
                        break
                    // 创建群
                    case 10:
                        // 排查是否重复注册群 标记
                        let isrevew = false
                        for (let i = 0; i < groups.length; i++) {
                            if (obj.groupId == groups[i].id) {
                                isrevew = true
                            }
                        }
                        if (isrevew == true) {
                            // 聊天室纯在
                            let group = groups.filter((item) => {
                                return item.id === obj.groupId
                            })[0]
                            let inGroup = group.users.filter((item) => {
                                return item.uid === obj.uid
                            })[0]
                            if (inGroup) {
                                // 在群里
                                boardcast({
                                    type: 1,
                                    date: moment().format(
                                        "YYYY-MM-DD HH:mm:ss"
                                    ),
                                    msg: "你已经是群成员了",
                                    users: users,
                                    groups: groups,
                                    uid: obj.uid,
                                    nickname: obj.nickname,
                                    bridge: obj.bridge,
                                })
                            } else {
                                group.users.push({
                                    uid: obj.uid,
                                    nickname: obj.nickname,
                                })
                                boardcast({
                                    type: 1,
                                    date: moment().format(
                                        "YYYY-MM-DD HH:mm:ss"
                                    ),
                                    msg:
                                        obj.nickname +
                                        "加入了群" +
                                        obj.groupName,
                                    users: users,
                                    groups: groups,
                                    uid: obj.uid,
                                    nickname: obj.nickname,
                                    bridge: obj.bridge,
                                })
                            }
                            isrevew == false
                        } else {
                            // 聊天室不纯在
                            groups.push({
                                id: obj.groupId,
                                name: obj.groupName,
                                users: [
                                    {
                                        uid: obj.uid,
                                        nickname: obj.nickname,
                                    },
                                ],
                            })
                            boardcast({
                                type: 1,
                                date: moment().format("YYYY-MM-DD HH:mm:ss"),
                                msg: obj.nickname + "创建了群" + obj.groupName,
                                users: users,
                                groups: groups,
                                uid: obj.uid,
                                nickname: obj.nickname,
                                bridge: obj.bridge,
                            })
                            isrevew == false
                        }
                        break
                    // 加入群
                    case 20:
                        let group = groups.filter((item) => {
                            return item.id === obj.groupId
                        })[0]
                        group.users.push({
                            uid: obj.uid,
                            nickname: obj.nickname,
                        })
                        boardcast({
                            type: 1,
                            date: moment().format("YYYY-MM-DD HH:mm:ss"),
                            msg: obj.nickname + "加入了群" + obj.groupName,
                            users: users,
                            groups: groups,
                            uid: obj.uid,
                            nickname: obj.nickname,
                            bridge: obj.bridge,
                        })
                        break
                    // 发送消息
                    default: // 保存信息
                        boardcast({
                            type: 2,
                            date: new Date(),
                            msg: obj.msg,
                            uid: obj.uid,
                            nickname: obj.nickname,
                            bridge: obj.bridge,
                            groupId: obj.groupId,
                            status: 1,
                        })
                        break
                }
            })
            conn.on("close", function (code, reason) {
                console.log("关闭连接")
            })
            conn.on("error", function (code, reason) {
                console.log("异常关闭")
            })
            console.log("WebSocket建立完毕")
        })
        .listen(8001)
    // WebSocket处理结束
}
