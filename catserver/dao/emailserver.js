// 引用发送邮件插件
var nodemailer = require('nodemailer')
// 引入证书
var credentials = require('../config/credentials')
// 创建传输方式
var transporter = nodemailer.createTransport({
    service:'qq',
    auth:{
        user:credentials.qq.user,
        pass:credentials.qq.pass
    }
})
// 注册发送邮件给用户
exports.emailSignUp = function(email,res){
    // 发送信息
    let options = {
        from: '3327372784@qq.com',
        to:email,
        subject:'感谢您的注册',
        html:'<span>欢迎加入我们</span><a href="http://localhost:8080/">点击</a>'
    }
    // 发送邮件
    transporter.sendMail(options,function(err,msg){
        if(err){
            res.send('邮箱发送失败')
            console.log(err);
        }else{
            res.send('邮箱发送成功')
            console.log('邮箱发送成功!')
        }
    })
}