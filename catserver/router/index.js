var dbserver = require("../dao/dbserver")
// 引入邮箱发送方法
var emailserver = require("../dao/emailserver")
const singin = require("../server/signin")
const signup = require("../server/singup")
const search = require("../server/search")
const user = require("../server/usetdetial")
const friend = require("../server/friend")
const index = require("../server/index")
const group = require("../server/group")
module.exports = function (app) {
    // 邮箱测试
    app.post("/mail", (req, res) => {
        let mail = req.body.mail
        emailserver.emailSignUp(mail, res)
        // console.log(mail)
        // res.send(mail)
    })
    // 注册
    app.post("/singup/add", (req, res) => {
        signup.signup(req, res)
    })
    // 用户或者邮箱是否占用
    app.post("/singup/judge", (req, res) => {
        signup.judgeValue(req, res)
    })
    // 登录
    app.post("/singin/math", (req, res) => {
        singin.singIn(req, res)
    })
    // 搜索用户
    app.post("/search/user", (req, res) => {
        search.searchUser(req, res)
    })
    // 判断是否为好友
    app.post("/search/isfriend", (req, res) => {
        search.isFriend(req, res)
    })
    // 搜索群
    app.post("/search/group", (req, res) => {
        search.searchGroup(req, res)
    })
    // 搜索用户
    app.post("/search/isingroup", (req, res) => {
        search.isInGroup(req, res)
    })
    // 用户详情
    app.post("/user/detail", (req, res) => {
        user.userDetail(req, res)
    })
    // 用户信息修改
    app.post("/user/update", (req, res) => {
        user.userUpdate(req, res)
    })
    // 好友昵称修改
    app.post("/user/updatamarkname", (req, res) => {
        user.updataMarkName(req, res)
    })
    // 好友昵称获取
    app.post("/user/getmarkname", (req, res) => {
        user.getMarkName(req, res)
    })
    // 好友操作
    // 申请好友
    app.post("/friend/applyfriend", (req, res) => {
        friend.applyFriend(req, res)
    })
    // 更新好友状态
    app.post("/friend/updateFriendStat", (req, res) => {
        friend.updateFriendState(req, res)
    })
    // 删除好友 拒绝好友
    app.post("/friend/deleteFriend", (req, res) => {
        friend.deleteFriend(req, res)
    })
    // 主页
    //获取好友
    app.post("/index/getfriend", (req, res) => {
        index.getFriend(req, res)
    })
    // 获取最后一条消息
    app.post("/index/getlastmsg", (req, res) => {
        index.getLastMsg(req, res)
    })
    //
    // 获取好友未读消息数
    app.post("/index/unreadmsg", (req, res) => {
        index.unresdMsg(req, res)
    })
    // 好友消息已读
    app.post("/index/updatamsg", (req, res) => {
        index.updateMsg(req, res)
    })
    // 获取群列表
    app.post("/index/getgroup", (req, res) => {
        index.getGroup(req, res)
    })
    // 获取群主的群
    app.post("/index/getqunzungroup", (req, res) => {
        index.getqunzhuGroup(req, res)
    })
    // 按要求获取群消息
    app.post("/index/getlastgroupmsg", (req, res) => {
        index.getOneGroupMsg(req, res)
    })
    //  获取最后一天群消息数
    app.post("/index/updategroupmsg", (req, res) => {
        index.updateGroupMsg(req, res)
    })
    // 获取一对一所有聊天消息
    app.post("/friend/allmag", (req, res) => {
        friend.getAllMsg(req, res)
    })
    // 一对一发消息
    app.post("/friend/senonemsg", (req, res) => {
        friend.sendOneMsg(req, res)
    })
    // 拿到用户名子
    app.post("/friend/getusername", (req, res) => {
        friend.getUserName(req, res)
    })
    // 更新用户信息
    app.post("/userdeatil/update", (req, res) => {
        user.update(req, res)
    })
    // 创建群
    app.post("/group/createGroupe", (req, res) => {
        group.createGroup(req, res)
    })
    // 拿到群名
    app.post("/group/getGroupName", (req, res) => {
        group.getGroupName(req, res)
    })
    // 获取群消息
    app.post("/group/getGroupMsg", (req, res) => {
        group.getGroupMsg(req, res)
    })
    // 发送群消息
    app.post("/group/snedGroupMsg", (req, res) => {
        group.sendOneGroupMsg(req, res)
    })
    // 获取群成员
    app.post("/group/getgroupMeber", (req, res) => {
        group.getgroupMeber(req, res)
    })
    app.post("/group/getAllGroup", (req, res) => {
        index.getAllGroup(req, res)
    })
    // 获取这个群里的所有成员
    app.post("/group/getgroupMeberDetail", (req, res) => {
        group.getgroupMeberDetail(req, res)
    })
    // 获取群主详细信息
    app.post("/group/getgroupMeberqnzuDetail", (req, res) => {
        group.getgroupMeberqnzuDetail(req, res)
    })
}
