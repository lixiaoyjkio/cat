var mongoose = require("mongoose")

var db = require("../config/db")

var Schema = mongoose.Schema

var SchemaUser = new Schema({
    //用户表： id，用户名，邮箱，性别，电话，密码，注册时间，签名，头像链接，
    name: { type: String },
    pwd: { type: String },
    email: { type: String },
    sex: { type: String ,default:'asexual'},
    birth: { type: Date },
    phone: { type: Number },
    explain: { type: String },
    imgurl: { type: String ,default:'user.png'},
    time: { type: Date },
})
// 好友表 id， 用户id，好友id，生成时间，好友状态（0表示申请中，1表示已为好友，2表示申请发送方对方未同意）
var FriendSchema = new Schema({
    userId:{type:Schema.Types.ObjectId,ref:'User'},
    friendId:{type:Schema.Types.ObjectId,ref:'User'},
    state:{type:String}, 
    time:{type:Date},
    lastTime:{type:Date}
})
// 一对一消息表：id，发送者id，接收者ID，发送内容，内容类型（0表示文字，1表示图片链接，2表示音频链接），发送时间，消息状态（0已读，1未读，）
var MessageSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: "User" },
    friendId: { type: Schema.Types.ObjectId, ref: "User" },
    message: { type: String },
    types:{type:String},
    time:{type:Date},
    state:{type:Number}
})
// 群表：id，群主id，群名，群头像链接，群公告，群创建时间
var GroupSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: "User" },
    name:{type:String},
    imgurl: { type: String, default:'group.png' },
    time: { type: Date },
    notice:{type:String}
})
// 成员表： Id，群id，用户id，群名，加入时间，未读消息数，是否屏蔽群消息（0不屏蔽，1屏蔽）
var GroupUserSchema = new Schema({
    groupId: { type: Schema.Types.ObjectId, ref: "Group" },
    userId:{type:Schema.Types.ObjectId,ref:'User'},
    name: { type: String },
    tip:{type:Number,default:0},
    time: { type: Date },
    last:{type:Date}, // 根据最后时间进行排序
    shield:{type:Number}
})
// 群消息表： Id， 群id，发送内容，内容类型（0文字，1图片链接，2音频链接。。。），发送时间
var GroupMsgSchema = new Schema({
    groupId: { type: Schema.Types.ObjectId, ref: "Group" },
    userId: { type: Schema.Types.ObjectId, ref: "User" },
    message:{type:String},
    types:{type:String},
    time: { type: Date },
})
module.exports = db.model("User", SchemaUser)
module.exports = db.model("Friend", FriendSchema)
module.exports = db.model("Message", MessageSchema)
module.exports = db.model("Group", GroupSchema)
module.exports = db.model("GroupUser", GroupUserSchema)
module.exports = db.model("GroupMsg", GroupMsgSchema)

