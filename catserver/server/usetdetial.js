var dbserver = require("../dao/dbserver")

// 详情
exports.userDetail = function (req, res) {
    let uid = req.body.uid
    dbserver.userDetail(uid, res)
}
// 用户信息修改
exports.updataMarkName = function (req, res) {
    let data = req.body
    dbserver.userUpdate(data, res)
}
// 修改好友昵称
exports.getMarkName = function (req, res) {
    let data = req.body
    dbserver.getMarkName(data, res)
}
// 更新用户信息
exports.update = function (req, res) {
    let data = req.body
    console.log(data)
    dbserver.updateSmhima(data, res)
}
