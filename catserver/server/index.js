// 主页
var dbserver =  require('../dao/dbserver')
// 获取好友列表
exports.getFriend = function(req,res){
    let data = req.body
    dbserver.getUsers(data,res)
}
// 获取最后一条消息
exports.getLastMsg = function(req,res){
    let data = req.body
    dbserver.getOneMsg(data,res)
}
// 获取好友未读消息数
exports.unresdMsg = function(req,res){
    let data = req.body;
    dbserver.unreadMsg(data,res)
}
// 好友消息标已读
exports.updateMsg = function (req, res) {
    let data = req.body
    dbserver.updateMsg(data, res)
}
// 获取所有的群
exports.getAllGroup = function (req, res) {
    let data = req.body
    dbserver.getAllGroup(data, res)
}
// 获取群列表
exports.getGroup = function (req, res) {
    let data = req.body
    dbserver.getGroup(data, res)
}
// 获取群主的群
exports.getqunzhuGroup = function (req, res) {
    let data = req.body
    dbserver.getqunzhuGroup(data, res)
}
// 按要求获取群消息
exports.getOneGroupMsg = function (req, res) {
    let data = req.body
    dbserver.getOneGroupMsg(data, res)
}
//  获取最后一天群消息数
exports.updateGroupMsg = function (req, res) {
    let data = req.body
    dbserver.updateGroupMsg(data, res)
}