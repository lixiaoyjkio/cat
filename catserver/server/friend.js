var dbserver = require('../dao/dbserver')

// 好友申请
exports.applyFriend = function(req,res){
    let data = req.body
    dbserver.applyFriend(data,res)
}
// 更新好友状态
exports.updateFriendState = function (req, res) {
    let data = req.body
    dbserver.updateFriendState(data, res)
}
// 删除好友或者拒绝好友申请
exports.deleteFriend = function (req, res) {
    let data = req.body
    dbserver.deleteFriend(data, res)
}
// 获取一对一所有聊天消息
exports.getAllMsg = function(req,res){
    let data = req.body
    dbserver.getAllmsg(data,res)
}

exports.sendOneMsg = function(req,res){
    let data  = req.body
    dbserver.sendOneMsg(data,res)
}

// 拿到用户名 
exports.getUserName = function(req,res){
    let data = req.body
    dbserver.getUserName(data,res)
}