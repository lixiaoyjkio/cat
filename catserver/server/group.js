var dbserver = require("../dao/dbserver")

// 好友申请
exports.createGroup = function (req, res) {
    let data = req.body
    
    dbserver.createGroupUser(data, res)
}
// 获取群消息
exports.getGroupName = function (req, res) {
    let data = req.body
    // console.log(data)
    dbserver.getGroupName(data, res)
}

// 发送群消息
exports.sendOneGroupMsg = function (req, res) {
    let data = req.body
    // console.log(data)
    dbserver.sendOneGroupMsg(data, res)
}
// 获取群成员
exports.getgroupMeber = function (req, res) {
    let data = req.body
    // console.log(data)
    dbserver.getgroupMeber(data, res)
}
// 获取群消息
exports.getGroupMsg = function(req,res){
    let data = req.body
    dbserver.getGroupMsg(data, res)
}
// 获取这个群里的所有成员
exports.getgroupMeberDetail = function (req, res) {
    let data = req.body
    dbserver.getgroupMeberDetail(data, res)
}
// 获取群主详细信息
exports.getgroupMeberqnzuDetail = function (req, res) {
    let data = req.body
    dbserver.getgroupMeberqnzuDetail(data, res)
}