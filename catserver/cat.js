const express = require("express")
const app = express()
const port = 3000
const bodyParser = require("body-parser") // 引入 dody-parser
const jwt = require("./dao/jwt")
// 允许跨域
const ws = require("nodejs-websocket") // 启动socket
require("./dao/websocket")(ws)

app.all("*", function (req, res, next) {
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin", "*")
    //允许的header类型
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With,Content-type,Accept"
    )
    //跨域允许的请求方式
    res.header("Access-Control-Allow-Credentials", true)
    res.header("Access-Control-Allow-Methods", "DELETE,PUT,POST,GET,OPTIONS")
    res.header("X-Powered-By", " 3.2.1")
    res.header("Content-Type", "application/json;charset=utf-8")
    if (req.method.toLowerCase() === "options")
        res.send({ status: 200 }) //让options尝试请求快速结束
    else next()
})
// 限制大小
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }))
// 解析前端数据
app.use(bodyParser.json())
// 获取静态路径
app.use(express.static(__dirname + "/data"))

require("./router/index")(app) // 引入路由文件
require("./router/files")(app) // 引入文件处理路由

console.log('访问成功了！')
// 跨域问题
app.all("*", function (req, res, next) {
    res.header(
        "Access-Control-Allow-Origin",
        "http://localhost:8080",
        "http://10.16.51.78:3000"
    ) //仅支持配置一个域名
    res.header(
        "Access-Control-Allow-Headers",
        "Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild"
    )
    res.header(
        "Access-Control-Allow-Methods",
        "PUT, POST, GET, DELETE, OPTIONS"
    )
    res.header("Access-Control-Allow-Credentials", true) //允许客户端携带验证信息
    next()
})

// token 判断
app.use(function (req, res, next) {
    if (typeof req.body.token != "undefined") {
        let token = req.body.token
        let tokenMath = jwt.verifyToken(token)
        if (tokenMath == 1) {
            next()
        } else {
            res.send({ status: 300 })
        }
    } else {
        next()
    }
})

app.use(function (req, res, next) {
    let err = new Error("not Found")
    err.status = 404
    next(err)
})
app.use(function (req, res, next) {
    res.status(err.status || 500)
    res.send(err.message)
})

app.listen(port, () => console.log(`您已经启动端口： ${port}!`))
