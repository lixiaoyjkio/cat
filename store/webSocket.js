

const value = uni.getStorageSync('user')
let vm = this

if (window.WebSocket) {
	vm.socket = new WebSocket("ws://localhost:8001")
	let socket = vm.socket
	socket.onopen = function(e) {
		console.log("已连接")
		vm.sendMessage(1)
	}
	socket.onclose = function(e) {
		console.log("服务器关闭");
	};
	socket.onerror = function() {
		console.log("连接出错");
	};
	// 接收服务器的消息
	socket.onmessage = function(e) {
		let message = JSON.parse(e.data)
		console.log(message)
	};
}
