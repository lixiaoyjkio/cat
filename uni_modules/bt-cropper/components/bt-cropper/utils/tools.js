export function getTouchPoints(touchs) {
	return Array.from(touchs).map(ev => {
		return [ev.clientX, ev.clientY]
	})
}
export function debounce(fn, wait = 200) {
	var timer = null;
	console.log('debounce')
	return function() {
		if (timer !== null) {
			clearTimeout(timer);
		}
		timer = setTimeout(fn, wait);
	}
}

export function assetImageZoom(originWidth, showWidth, imageScale, scale, maxZoom) {
	const zoom = imageScale * scale
	const showSize = showWidth * zoom
	const maxSize = originWidth * maxZoom
	if (maxSize < showSize) {
		return scale
	} else {
		return maxSize / showWidth
	}
}
