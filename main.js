import App from './App'
// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'


// 服务器地址
//本地地址
Vue.prototype.serverUrl = 'http://localhost:3000'
// 局域网地址
// Vue.prototype.serverUrl = 'http://10.16.51.78:3000'
require("./websocket.js")


const app = new Vue({
	...App
})

app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
